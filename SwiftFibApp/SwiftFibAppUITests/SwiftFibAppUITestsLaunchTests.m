//
//  SwiftFibAppUITestsLaunchTests.m
//  SwiftFibAppUITests
//
//  Created by Martin Kompan on 22/12/2022.
//

#import <XCTest/XCTest.h>

@interface SwiftFibAppUITestsLaunchTests : XCTestCase

@end

@implementation SwiftFibAppUITestsLaunchTests

+ (BOOL)runsForEachTargetApplicationUIConfiguration {
    return YES;
}

- (void)setUp {
    self.continueAfterFailure = NO;
}

- (void)testLaunch {
    XCUIApplication *app = [[XCUIApplication alloc] init];
    [app launch];

    XCTAttachment *attachment = [XCTAttachment attachmentWithScreenshot:XCUIScreen.mainScreen.screenshot];
    attachment.name = @"Launch Screen";
    attachment.lifetime = XCTAttachmentLifetimeKeepAlways;
    [self addAttachment:attachment];
}

@end
