//
//  SwiftFibAppUITests.m
//  SwiftFibAppUITests
//
//  Created by Martin Kompan on 22/12/2022.
//

#import <XCTest/XCTest.h>

@interface SwiftFibAppUITests : XCTestCase

@end

@implementation SwiftFibAppUITests

- (void)setUp {
    self.continueAfterFailure = NO;
}

- (void)tearDown {
}

- (void)testExample {
    XCUIApplication *app = [[XCUIApplication alloc] init];
    [app launch];
    
    [app.buttons[@"Start"] tap];
    
    [[app.textFields elementBoundByIndex:0] tap];
    
    [[app.textFields elementBoundByIndex:0] typeText:@"10\n"];
    
    [app.buttons[@"Submit"] tap];
    
    XCTAssert(app.staticTexts[@"F(10) = 34"].exists);
}

@end
