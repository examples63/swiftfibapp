//
//  FirstScreen.m
//  SwiftFibApp
//
//  Created by Martin Kompan on 22/12/2022.
//

#import "FirstScreen.h"
#import "SecondScreen.h"


@interface FirstScreen ()

@end

@implementation FirstScreen

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (IBAction)clickStartButton:(UIButton *)sender {
    SecondScreen *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"SecondScreen"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)inputField:(id)sender {
}
@end
