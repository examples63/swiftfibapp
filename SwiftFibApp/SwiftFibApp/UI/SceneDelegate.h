//
//  SceneDelegate.h
//  SwiftFibApp
//
//  Created by Martin Kompan on 22/12/2022.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

