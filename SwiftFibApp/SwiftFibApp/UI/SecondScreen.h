//
//  SecondScreen.h
//  SwiftFibApp
//
//  Created by Martin Kompan on 22/12/2022.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SecondScreen : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UITextField *inputField;

@end

NS_ASSUME_NONNULL_END
