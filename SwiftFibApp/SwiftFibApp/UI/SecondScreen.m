//
//  SecondScreen.m
//  SwiftFibApp
//
//  Created by Martin Kompan on 22/12/2022.
//

#import "SecondScreen.h"

#import <SwiftFibFW/SwiftFibFW.h>

@interface SecondScreen ()

@end

@implementation SecondScreen

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    self.submitButton.enabled = false;
    self.title = @"F(n)";
}

- (void)checkInput:(NSString*)newN {
    int n = newN.intValue;
    if (  n > 0 && n < 35 ) { // TODO: for n > 34 the "int" will overflow, use "unsigned long"
        self.submitButton.enabled = true;
    }else {
        self.submitButton.enabled = false;
    }
}

- (IBAction)numberChanged:(UITextField *)sender {
    [self checkInput:sender.text];
}

- (IBAction)submit:(UIButton *)sender {
    [self evaluate: _inputField.text.intValue];
}

- (void) evaluate:(int) n {
    
    FibCounter *counter = [[FibCounter alloc] init];
    
    unsigned long result = [counter fibonacciBinet:n-1];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"F(%d) = %ld", n, result] message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Dissmiss" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
