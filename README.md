# SwiftFibApp Objective-C

[**Author:** Martin Kompan](https://gitlab.com/examples63/mkuseful/-/blob/main/certs/Programming_certificates.md)

Tested using **Xcode 14.1**

Architecture: **MVC** (MVVMC not required/necessary)

Using: **Objective-C 2.0**

iOS deployment target: **16.0**

## Swift package integration 

### Integration option 1: [Import as Swift package into Xcode](https://developer.apple.com/documentation/xcode/adding-package-dependencies-to-your-app)

(only manual updates, for auto-update use option 2 ) Open your Xcode Project (or Workspace), click on **File > Swift Packages > Add Package Dependency** and enter package git  [repository URL](https://gitlab.com/examples63/swiftfib.git) ( https://gitlab.com/examples63/swiftfib.git ) into **"Search or Enter Package URL"** input textfield.


### Integration option 2: Git subrepository

#### Add subrepository into git
```
git submodule add https://gitlab.com/examples63/swiftfib.git
    
git add .
    
git commit -m "Add GitHub submodule"
```
#### Add using Xcode's Swift Package Manager

Go to **Xcode** -> press **File** -> **Add packages...** -> **"Add local..."** button -> select the submodule's root git folder -> press **"Add package"** button

**Project's name** in navigation -> Project's **target** -> **General** -> find **"Frameworks, Libraries and Embedded Content"** list -> **"+"** button -> select **"SwiftFib"** library -> **"Add"**


Now you should be able to use and build **SwiftFib** package with your project.

**For auto-updating the package:**


**Project's name** in navigation -> Project's **target** -> **Build Phases** -> **New Run Script Phase** -> and add into it this:

```
git submodule foreach \

git pull origin main
```
